{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}
<!DOCTYPE html>
<html lang="en">


<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <title>Dashtreme - Multipurpose Bootstrap4 Admin Template</title>
  <!--favicon-->
  <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
  <!-- Bootstrap core CSS-->
  <link href="{{ asset("public/assets/css/bootstrap.min.css")}}" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="{{ asset("public/assets/css/animate.css")}}" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="{{ asset("public/assets/css/icons.css")}}" rel="stylesheet" type="text/css"/>
  <!-- Custom Style-->
  <link href="{{ asset("public/assets/css/app-style.css")}}" rel="stylesheet"/>
  
</head>

<body class="bg-theme bg-theme1">

    <!-- Start wrapper-->
    <div id="wrapper">

      <div class="loader-wrapper"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div>

      <div class="loader-wrapper"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div>
      <div class="card card-authentication1 mx-auto my-5">
        <div class="card-body">
           <div class="card-content p-2">
            <div class="text-center">
                <img src="{{ asset('public/assets/images/logo-icon.png') }}" alt="logo icon">
            </div>
            <div class="card-title text-uppercase text-center py-3">Sign In</div>
            <form  method="POST" action="{{ route('login') }}">
               @csrf
               <div class="form-group">
                  <label for="exampleInputUsername" class="sr-only">Username</label>
                  <div class="position-relative has-icon-right">
                      <input  type="email"  id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} input-shadow" name="email" value="{{ old('email') }}" required autofocus placeholder="Enter E-Mail">

                      <div class="form-control-position">
                          <i class="icon-user"></i>
                      </div>
                      @if ($errors->has('email'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>




            </div>
            <div class="form-group">
              <label for="exampleInputPassword" class="sr-only">Password</label>
              <div class="position-relative has-icon-right">
                  <input id="password" type="password"  class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} input-shadow"  name="password" required placeholder="Enter Password">
                  <div class="form-control-position">
                      <i class="icon-lock"></i>
                  </div>
                  @if ($errors->has('password'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
        </div>




        <div class="form-row">
           <div class="form-group col-6">
             <div class="icheck-material-white">
                <input type="checkbox" id="user-checkbox" checked="" />
                <label for="user-checkbox">Remember me</label>
            </div>
        </div>
        <div class="form-group col-6 text-right">
          <a href="authentication-reset-password.html">Reset Password</a>
      </div>
  </div>
  <button type="submit" class="btn btn-light btn-block">Sign In</button>
  <div class="text-center mt-3">Sign In With</div>

  <div class="form-row mt-4">
      <div class="form-group mb-0 col-6">
         <button type="button" class="btn btn-light btn-block"><i class="fa fa-facebook-square"></i> Facebook</button>
     </div>
     <div class="form-group mb-0 col-6 text-right">
      <button type="button" class="btn btn-light btn-block"><i class="fa fa-twitter-square"></i> Twitter</button>
  </div>
</div>

</form>
</div>
</div>
<div class="card-footer text-center py-3">
    <p class="text-warning mb-0">Do not have an account? <a href="authentication-signup.html"> Sign Up here</a></p>
</div>
</div>



</div><!--wrapper-->

<!-- Bootstrap core JavaScript-->
<script src="{{ asset("public/assets/js/jquery.min.js")}}"></script>
<script src="{{ asset("public/assets/js/popper.min.js")}}"></script>
<script src="{{ asset("public/assets/js/bootstrap.min.js")}}"></script>

<!-- sidebar-menu js -->
<script src="{{ asset("public/assets/js/sidebar-menu.js")}}"></script>

<!-- Custom scripts -->
<script src="{{ asset("public/assets/js/app-script.js")}}"></script>

</body>

</html>
