@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header"><i class="fa fa-table"></i> Data Exporting
				<a onclick="addForm()" class="btn btn-success" style="float: right;" data-toggle="modal" data-target="#exampleModal">add-new</a>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table id="default-datatable" class="table table-bordered">
						<thead>
							<tr>
								<th>ID</th>
								<th>Title</th>
								<th>Author</th>
								<th>description</th>
								<th>Action</th>

							</tr>
						</thead>
						<tbody>

						</tbody>

					</table>
				</div>
			</div>
		</div>
	</div>
</div><!-- End Row-->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form method="post">	
       		@csrf {{ method_field('POST') }} {{-- ajax use korle {{ method_field('POST') }} eti use kora lagbe --}}
       			  <input type="hidden" name="id" id="id">
       	<input type="text" id="title" name="title" placeholder="title" class="form-control"><br>			
       	<input type="text" id="author" name="author" placeholder="author" class="form-control"><br>			
       <textarea class="form-control" id="description" name="description" placeholder="description"></textarea>		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="insertbutton"></button>
    </div>
      </div>
    </div>
  </div>
</div>

@endsection