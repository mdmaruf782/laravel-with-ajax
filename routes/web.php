<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('profile', function () {
    // Only verified users may enter...
})->middleware('verified');

Route::group(['middleware' => 'verified'],function(){

Route::get('/mail', function(){
	echo "mail";
})->name('mail');

});


Route::resource('post', 'PostController');

Route::get('All-post', 'PostController@all_post')->name('all.post');

/*Route::get('/mail', function(){
	echo "mail";
})->name('mail')->middleware('verified');*/

